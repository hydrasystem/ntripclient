/**
 * @file netlo.c
 * @brief This is implementation of tcp client for ntripclient. It is intended
 * to have very similar interface to serial.c.
 * 
 * Netlo is from Network Local. Not to be confused with many option for network
 * connecting with ntripcaster. This is intended to tail the same way with GNSS
 * like over serial, but using network link.
 * 
 */


#include "netlo.h"

/**
 * @brief Handler for SIGPIPE signal.
 * 
 * Used to catch disconnects in case we have non-blocking connections.
 * 
 * @todo Check if that doesn't interfere with rest of code.
 * @param descr Socket.
 */
void sigpipe_handler(int descr) {
    // fprintf(stderr, "Got SIGPIPE %d\n", descr);
}

/**
 * @brief Initialises this module.
 * 
 * This will call @ref NetloStateMachine and hopefully progress all the way
 * to @ref netlo::READY.
 * 
 * @param nl Pointer to main structure.
 * @param address Address to connect to.
 * @param port Port to connect to.
 * @return const char* Error or NULL if no error to report.
 */
const char * NetloInit(struct netlo * nl, const char * address, 
    unsigned short int port) {
    // Set up SIGPIPE handler, for handlink disconnected sockets
    sigaction(SIGPIPE, &(struct sigaction){sigpipe_handler}, NULL);
    // Prepare address structure
    // Just in case
    memset(&nl->addr, 0, sizeof(nl->addr));
    nl->addr.sin_family = AF_INET; 
    nl->addr.sin_port = htons(port); 
    // Convert IPv4 and IPv6 addresses from text to binary form 
    // For IPv6 use AF_INET6
    switch (inet_pton(AF_INET, address, &(nl->addr.sin_addr))){ 
        case 0:
            fprintf(stderr, "Invalid address\n"); 
            nl->state = ERROR;
            return "Invalid address";
        case -1:
            fprintf(stderr, "Address not supported, errno=%d\n", errno);
            nl->state = ERROR;
            return "Address not supported";
    } 
    nl->state = INIT;
    return NetloStateMachine(nl);
}


/**
 * @brief Creates the socket. This is normally called by @ref NetloStateMachine.
 * 
 * After that state will be set to @ref netlo::RETRY and connection will be 
 * connected.
 * 
 * @param nl Pointer to main structure.
 * @return const char* Error or NULL if no error to report.
 */
const char * NetloOpen(struct netlo * nl){
    // Type can be: SOCK_DGRAM - UDP, SOCK_STREAM - TCP
    // Domain: AF_INET - IPv4,  AF_INET6 - IPv6
    // Protocol ip 0, see /etc/protocol
    nl->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // On success, a file descriptor for the new socket is returned.  On
    // error, -1 is returned, and errno is set appropriately.
    if (nl->sockfd == -1) {
        nl->state = ERROR;
        switch (errno){
            case EACCES:
                // Permission to create a socket of the specified type and/or 
                // protocol is denied.
                fprintf(stderr, "Permission to create socket denied\n");
                return "Permission to create socket denied";
            case EAFNOSUPPORT:
                // The implementation does not support the specified address
                // family.
                fprintf(stderr, "Unsupported address\n");
                return "Unsupported address";
            case EINVAL:
                // Unknown protocol, or protocol family not available or invalid 
                // flags in type.
                fprintf(stderr, "Invalid protocol, family or flags\n");
                return "Invalid protocol, family or flags";
            case EMFILE:
                // The per-process limit on the number of open file descriptors 
                // has been reached or the system-wide limit on the total number 
                // of open files has been reached.
                fprintf(stderr, "Limit of file descriptors reached\n");
                return "Limit of file descriptors reached";
            case ENOBUFS:
            case ENOMEM:
                // Insufficient memory is available.  The socket cannot be 
                // created until sufficient resources are freed.
                fprintf(stderr, "Insufficient memory\n");
                return "Insufficient memory";
            case EPROTONOSUPPORT:
                // The protocol type or the specified protocol is not supported 
                // within this domain.
                fprintf(stderr, "Type or protocol not supported\n");
                return "Type or protocol not supported";
            case ESOCKTNOSUPPORT:
                fprintf(stderr, "Socket not supported\n");
                return "Socket not supported";
            default:
                fprintf(stderr, "Unable to open socket, errno=%d\n", errno);        
                return "Unknown error opening socket";
        }
    }
    int optval;
    socklen_t optlen = sizeof(optval);
    // With SOL_TCP
    // TCP_KEEPCNT: overrides tcp_keepalive_probes
    // TCP_KEEPIDLE: overrides tcp_keepalive_time
    // TCP_KEEPINTVL: overrides tcp_keepalive_intvl
    // With SOL_SOCKET
    // SO_KEEPALIVE
    // Note: Keepalive might not work in all situations. 
    // if(getsockopt(nl->sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, &optlen) < 0) {
    //     fprintf(stderr, "Error in getsockopt(), errno=%d", errno);
    //     close(nl->sockfd);
    //     nl->state = ERROR;
    //     return "Unable to getsockopt";
    // }
    // printf("SO_KEEPALIVE is %d\n", optval);
    // if(getsockopt(nl->sockfd, SOL_TCP, TCP_KEEPCNT, &optval, &optlen) < 0) {
    //     fprintf(stderr, "Error in getsockopt(), errno=%d", errno);
    //     close(nl->sockfd);
    //     nl->state = ERROR;
    //     return "Unable to getsockopt";
    // }
    // printf("TCP_KEEPCNT is %d\n", optval);
    // if(getsockopt(nl->sockfd, SOL_TCP, TCP_KEEPIDLE, &optval, &optlen) < 0) {
    //     fprintf(stderr, "Error in getsockopt(), errno=%d", errno);
    //     close(nl->sockfd);
    //     nl->state = ERROR;
    //     return "Unable to getsockopt";
    // }
    // printf("TCP_KEEPIDLE is %d\n", optval);
    // if(getsockopt(nl->sockfd, SOL_TCP, TCP_KEEPINTVL, &optval, &optlen) < 0) {
    //     fprintf(stderr, "Error in getsockopt(), errno=%d", errno);
    //     close(nl->sockfd);
    //     nl->state = ERROR;
    //     return "Unable to getsockopt";
    // }
    // printf("TCP_KEEPINTVL is %d\n", optval);

    
    // Turn on keepalive
    optval = 1;
    optlen = sizeof(optval);
    if(setsockopt(nl->sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
        fprintf(stderr, "Error in setsockopt(), errno=%d", errno);
        close(nl->sockfd);
        return "Unable to setsockopt";
    }
    
    optval = 5;
    optlen = sizeof(optval);
    if(setsockopt(nl->sockfd, SOL_TCP, TCP_KEEPINTVL, &optval, optlen) < 0) {
        fprintf(stderr, "Error in setsockopt(), errno=%d", errno);
        close(nl->sockfd);
        return "Unable to getsockopt";
    }
    return NULL;
}

/**
 * @brief Connects the socket.
 * 
 * This function is normally called with @ref NetloStateMachine, and progresses
 * state from @ref netlo_state::RETRY to netlo_state::READY.
 * 
 * @param nl Pointer to main structure.
 * @return const char* Error or NULL if no error to report.
 */
const char * NetloConnect(struct netlo * nl) {
    nl->connect = connect(nl->sockfd, (struct sockaddr *)&(nl->addr), 
                            sizeof(nl->addr));
    if (nl->connect == -1) {
        nl->state = ERROR;
        switch (errno) {
            case EACCES:
            case EPERM:
                // For UNIX domain sockets, which are identified by pathname:
                // Write permission is denied on the socket file, or search
                // permission is denied for one of the directories in the path
                // prefix.  (See also path_resolution(7).)
                // Or the user tried to connect to a broadcast address without
                // having the socket broadcast flag enabled or the connection
                // request failed because of a local firewall rule.
                fprintf(stderr, "Error accessing\n");
                return "Error accessing";
            case EADDRINUSE:
                // Local address is already in use.
                fprintf(stderr, "Local address is already in use\n");
                return "Local address is already in use.";
            case EADDRNOTAVAIL:
                // (Internet domain sockets) The socket referred to by sockfd had
                // not previously been bound to an address and, upon attempting
                // to bind it to an ephemeral port, it was determined that all
                // port numbers in the ephemeral port range are currently in use.
                // See the discussion of /proc/sys/net/ipv4/ip_local_port_range
                // in ip(7).
                fprintf(stderr, "Not in port range\n");
                return "Not in port range";
            case EAFNOSUPPORT:
                // The passed address didn't have the correct address family in
                // its sa_family field.
                fprintf(stderr, "Address family not supported\n");
                return "Address family not supported";
            case EAGAIN:
                // For nonblocking UNIX domain sockets, the socket is
                // nonblocking, and the connection cannot be completed
                // immediately.  For other socket families, there are
                // insufficient entries in the routing cache.
                fprintf(stderr, "Got EAGAIN\n");
                return "Got EAGAIN";
            case EALREADY:
                // The socket is nonblocking and a previous connection attempt
                // has not yet been completed.
                fprintf(stderr, "Previous attempt not completed\n");
                return "Previous attempt not completed";
            case EBADF:
                // Socket is not a valid open file descriptor.
                fprintf(stderr, "Wrong descriptor\n");
                return "Wrong descriptor";
            case ECONNREFUSED:
                // A connect() on a stream socket found no one listening on the
                // remote address.
                fprintf(stderr, "Connection refused\n");
                nl->state = RETRY;
                return "Connection refused";
            case EFAULT:
                // The socket structure address is outside the user's address
                // space.
                fprintf(stderr, "Address outside user address space\n");
                return "Address outside user address space";
            case EINPROGRESS:
                // The socket is nonblocking and the connection cannot be
                // completed immediately.  (UNIX domain sockets failed with
                // EAGAIN instead.)  It is possible to select(2) or poll(2) for
                // completion by selecting the socket for writing.  After
                // select(2) indicates writability, use getsockopt(2) to read the
                // SO_ERROR option at level SOL_SOCKET to determine whether
                // connect() completed successfully (SO_ERROR is zero) or
                // unsuccessfully (SO_ERROR is one of the usual error codes
                // listed here, explaining the reason for the failure).
                fprintf(stderr, "In progress\n");
                return "In progress";
            case EINTR:
                // The system call was interrupted by a signal that was caught;
                // see signal(7).
                fprintf(stderr, "Got signal\n");
                return "Got signal";
            case EISCONN:
                // The socket is already connected.
                fprintf(stderr, "The socket is already connected\n");
                return "The socket is already connected";
            case ENETUNREACH:
                // Network is unreachable.
                fprintf(stderr, "Network is unreachable\n");
                return "Network is unreachable";
            case ENOTSOCK:
                // The file descriptor sockfd does not refer to a socket.
                fprintf(stderr, "Descriptor does not refer to a socket\n");
                return "Descriptor does not refer to a socket.";
            case EPROTOTYPE:
                // The socket type does not support the requested communications
                // protocol.  This error can occur, for example, on an attempt to
                // connect a UNIX domain datagram socket to a stream socket.
                fprintf(stderr, "Requested communications protocol not supported\n");
                return "Requested communications protocol not supported";
            case ETIMEDOUT:
                // Timeout while attempting connection.  The server may be too
                // busy to accept new connections.  Note that for IP sockets the
                // timeout may be very long when syncookies are enabled on the
                // server.
                fprintf(stderr, "Timeout while attempting connection\n");
                return "Timeout while attempting connection";
            default:
                fprintf(stderr, "Unable to connect socket, errno=%d\n", errno);
                return "Unable to connect socket";
        }
    }
    return NULL;
}

/**
 * @brief Check state machine status. This is used after @ref NetloInit to 
 * change internal state machine states.
 * 
 * @param nl Pointer to main structure.
 * @return const char* Error or NULL if no error to report.
 */
const char *  NetloStateMachine(struct netlo * nl){
    if (nl->state == ERROR) {
        return "Error state set";
    }
    if (nl->state == INIT) {
        const char * ret = NetloOpen(nl);
        if (ret != NULL){
            // This is hard reconnect, probably server was closed
            return ret;
        } else {
            nl->state = RETRY;
        }
    }
    if (nl->state == RETRY) {
        const char * ret = NetloConnect(nl);
        if (ret != NULL){
            //Still unable to connect
            return ret;
        } else {
            nl->state = READY;
        }
    }
    return NULL;
}

/**
 * @brief Calles close on socket and sets state to @ref netlo_state::INIT.
 * 
 * It should be called when we have error that requires it. Should eventually
 * reconnect.
 * 
 * @param nl Pointer to main structure.
 */
void NetloFree(struct netlo * nl){
    close(nl->sockfd);
    nl->state = INIT;
}

/**
 * @brief Blocking read.
 * 
 * @param nl Pointer to main structure.
 * @param buffer Buffer to read data to.
 * @param size Size of data to be read. 
 * @return int Data read if everything went fine or -1 in case of error.
 */
int NetloReadBlock(struct netlo * nl, char *buffer, size_t size){
    if (NetloStateMachine(nl) != NULL) {
        return 0;
    }
    memset(buffer, 0, size);
    return recv(nl->sockfd, buffer, size, 0);
}


/**
 * @brief Non blocking read.
 * 
 * @param nl Pointer to main structure.
 * @param buffer Buffer to read data to.
 * @param size Size of data to be read. 
 * @return int Data read if everything went fine (can be zero, if there is 
 * nothing to read) or -1 in case of error.
 */
int NetloReadNoBlock(struct netlo * nl, char *buffer, size_t size){
    if (NetloStateMachine(nl) != NULL) {
        return 0;
    }
    memset(buffer, 0, size);
    int ret = recv(nl->sockfd, buffer, size, MSG_DONTWAIT);
    if (ret == -1){
        switch (errno) {
            // case EWOULDBLOCK - this is same as EAGAIN
            case EAGAIN:            
                //This is normal. We just have not received anything.
                return 0;                
            default:
                fprintf(stderr, "Error while receiving, errno=%d", errno);
                return -1;
        }
    }
    return ret;
}


/**
 * @brief Write data.
 * 
 * @param nl Pointer to main structure.
 * @param buffer Data to be written. 
 * @param size Size of data do be written.
 * @return int This is -1 in case of error or number of written bytes. If bytes
 * are not written, library can try to handle error (if it can), but data is
 * not beeing resend. 
 */
int NetloWrite(struct netlo * nl, const char *buffer, size_t size){
    if (NetloStateMachine(nl) != NULL) {
        return -1;
    }
    int ret = send(nl->sockfd, buffer, size, 0);
    if (ret == -1) {
        switch (errno) {
            case EPIPE:
                // Broken pipe, probably server disconnected
                // Force system to retry connection on next attempt of doing 
                // anythong.
                NetloFree(nl);
                return -1;
            default:
                fprintf(stderr, "Error while sending, errno=%d\n", errno);
                return -1;
        }
    }
    return ret;
}


