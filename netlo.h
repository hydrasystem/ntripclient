/**
 * @file netlo.h
 * @brief This is header of tcp client for ntripclient. It is intended
 * to have very similar interface to serial.c.
 */

#include <stddef.h>         // Definition for NULL
#include <errno.h>          // Errno access
#include <stdio.h>          // Error streams
#include <string.h>         // Memset
#include <unistd.h>         // For close
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h> 
#include <signal.h>

/**
 * @brief State machine allowe states. 
 */
typedef enum netlo_state {
    /** 
     * @brief Function @ref NetloInit() was called, but no connection 
     * was established. Address structure should be filled.
     */
    INIT,
    /**
     * @brief Ready for operation.
     */
    READY,
    /**
     * @brief There was and error that this library cannot handle internally.
     */
    ERROR,
    /**
     * @brief We are trying to reconnect for some reason.
     */
    RETRY
} netlo_state;

/** 
 * @brief Main structure with connection state. 
 */
struct netlo {
    /** Socket descriptor. */
    int sockfd; 
    /** Result of connect last call. */
    int connect;
    /** Internal state. */
    netlo_state state;
    /** Structure with address */
    struct sockaddr_in addr;
};

//Functions
const char * NetloInit(struct netlo *, const char *, unsigned short int);
const char * NetloOpen(struct netlo *);
const char * NetloConnect(struct netlo *);
const char * NetloStateMachine(struct netlo *);
void NetloFree(struct netlo *);
int NetloReadBlock(struct netlo *, char *, size_t);
int NetloReadNoBlock(struct netlo *, char *, size_t);
int NetloWrite(struct netlo *, const char *, size_t);
