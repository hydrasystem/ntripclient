
#include <unistd.h>
#include "acutest.h"
#include "../netlo.c"

/**
 * @brief Test for netlo.
 * @note This is not fully automatic now. It will ask at some point and then 
 * netcat has to be started 
 * @code
 * nc -lk localhost 11911
 * @end
 * 
 */
void test_create(void) {
    struct netlo nl;
    TEST_CHECK(NetloInit(&nl, "", 1) != NULL);
    TEST_CHECK(nl.state == ERROR);
    TEST_CHECK(NetloInit(&nl, "127.0.0.1", 1) != NULL);
    TEST_CHECK(nl.state == RETRY);
    TEST_CHECK(NetloInit(&nl, "127.0.0.1", 11811) != NULL);
    TEST_CHECK(nl.state == RETRY);
    TEST_CHECK(NetloWrite(&nl, "Ala ma kota", 12) == -1);
    NetloFree(&nl);
    // This is before netcat is started
    TEST_CHECK(NetloInit(&nl, "127.0.0.1", 11911) != NULL);
    printf("For this test we need netcat on port 11911\n");
    printf("Run now:\n");
    printf("nc -lk localhost 11911\n");    
    char buf[255];
    while(1) {
        int ret;
        ret = NetloWrite(&nl, "Ala ma kota\n\r", 14);
        TEST_CHECK(ret == 14 || ret == -1);
        ret = NetloReadNoBlock(&nl, buf, 255);
        if (ret>0){
            printf("%s\n", buf);
        }
        sleep(1);
    }
}

TEST_LIST = {
   { "create", test_create },
   { 0 }
};