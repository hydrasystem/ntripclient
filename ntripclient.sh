#!/bin/sh
#

. /etc/default/ntripclient.conf

# We expect something like this in config file
# MOUNTPOINT="base0"
# USER="user"
# PASSWORD="password"
# SERVER="server.domain.com"
# PORT="12345"
# DEVICE="/dev/ttyUSB0"
# BAUDRATE="9600"
# OTHER=
# LOCAL_SERVER="localhost"
# LOCAL_PORT="1010"

SLEEPTIME=5
sleep $SLEEPTIME
while true; 
do
  if [ -z "$LOCAL_SERVER" ]
  then
    ntripclient -s $SERVER -r $PORT -m $MOUNTPOINT -u $USER -p $PASSWORD -D $DEVICE -B $BAUDRATE $OTHER
  else
    # while ! </dev/tcp/$LOCAL_SERVER/$LOCAL_PORT ; do echo Port $LOCAL_PORT closed; sleep 2; done; echo Port $LOCAL_PORT available.
    # ntripclient -s $SERVER -r $PORT -m $MOUNTPOINT -u $USER -p $PASSWORD | nc $LOCAL_SERVER $LOCAL_PORT
    ntripclient -s $SERVER -r $PORT -m $MOUNTPOINT -u $USER -p $PASSWORD -q $LOCAL_SERVER -w $LOCAL_PORT $OTHER
  fi
  echo "Retry after " + $SLEEPTIME
  sleep $SLEEPTIME
done
